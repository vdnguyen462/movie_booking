import AdminLayout from '../components/Layout/AdminLayout';
import AddAdminMovie from '../pages/AdminMoviePage/AddAdminMovie/AddAdminMovie';
import AdminMoviePage from '../pages/AdminMoviePage/AdminMoviePage';
import AddAdminUser from '../pages/AdminUserPage/AddAdminUser/AddAdminUser';
import AdminUserPage from '../pages/AdminUserPage/AdminUserPage';

export const adminRoutes = [
    {
        url: '/admin-users',
        component: <AdminLayout Component={AdminUserPage} />,
    },
    {
        url: '/admin-add-users',
        component: <AdminLayout Component={AddAdminUser} />,
    },
    {
        url: '/admin-movies',
        component: <AdminLayout Component={AdminMoviePage} />,
    },
    {
        url: '/admin-add-movies',
        component: <AdminLayout Component={AddAdminMovie} />,
    },
];
