import { https } from './config';

export const adminService = {
    getUserList: () => {
        return https.get('/api/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP00');
    },
    deleteUser: (taiKhoan) => {
        return https.delete(`/api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`);
    },
    getSearchUser: (keyword) => {
        return https.get(`/api/QuanLyNguoiDung/TimKiemNguoiDung?MaNhom=GP01&tuKhoa=${keyword}`);
    },
    addUser: () => {
        return https.post('/api/QuanLyNguoiDung/ThemNguoiDung');
    },
};
