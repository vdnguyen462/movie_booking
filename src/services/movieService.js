import { https, GROUPID } from './config';

export const movieService = {
    getListFirm: () => {
        return https.get('/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP07');
    },

    getBannerFirm: () => {
        return https.get('/api/QuanLyPhim/LayDanhSachBanner');
    },

    getListMoviePerOnePage: (currenPage, itemPerPage) => {
        return https.get(
            `/api/QuanLyPhim/LayDanhSachPhimPhanTrang?maNhom=GP07&soTrang=${currenPage}&soPhanTuTrenTrang=${itemPerPage}`,
        );
    },

    getDataTheater: () => {
        return https.get('/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP07');
    },

    getDataShowTimesMovie: (id) => {
        return https.get(`/api/QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${id}`);
    },
    getListMovie: () => {
        return https.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUPID}`);
    },
    deleteMovie: (MaPhim) => {
        return https.delete(`/api/QuanLyPhim/XoaPhim?MaPhim=${MaPhim}`);
    },
    getSearchMovie: (MaPhim) => {
        return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${MaPhim}`);
    },
    getAddImageMovie: (formData) => {
        return https.post('/api/QuanLyPhim/ThemPhimUploadHinh', formData);
    },
};

// import { GROUPID, https } from './config';

// export const movieService = {
//     getListMovie: () => {
//         return https.get(`/api/QuanLyPhim/LayDanhSachPhim?maNhom=${GROUPID}`);
//     },
//     deleteMovie: (MaPhim) => {
//         return https.delete(`/api/QuanLyPhim/XoaPhim?MaPhim=${MaPhim}`);
//     },
//     getSearchMovie: (MaPhim) => {
//         return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${MaPhim}`);
//     },
//     getAddImageMovie: (formData) => {
//         return https.post('/api/QuanLyPhim/ThemPhimUploadHinh', formData);
//     },
// };
