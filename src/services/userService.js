import { https } from './config';

export const userService = {
    postLogin: (loginForm) => {
        return https.post('/api/QuanLyNguoiDung/DangNhap', loginForm);
    },

    postSignUp: (signUpForm) => {
        return https.post('/api/QuanLyNguoiDung/DangKy', signUpForm);
    },
};
