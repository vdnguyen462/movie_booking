import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import UserSpinner from './components/UserSpinner/UserSpinner';
import UserDefaultLayout from './layouts/UserDefaultLayout/UserDefaultLayout';
import DetailMovie from './pages/DetailMovie/DetailMovie';
import Spinner from './components/Spinner/Spinner';
import HomePage from './pages/HomePage/HomePage';
import LoginPage from './pages/LoginPage/LoginPage';
import { adminRoutes } from './routes/adminRoutes';

function App() {
    return (
        <div>
            <UserSpinner />
            <Spinner />

            <BrowserRouter>
                <Routes>
                    {adminRoutes.map(({ url, component }) => {
                        return <Route path={url} key={url} element={component} />;
                    })}
                    <Route path="/" element={<UserDefaultLayout Component={HomePage} />} />
                    <Route path="/login" element={<LoginPage />} />
                    <Route path="/detail-movie/:id" element={<UserDefaultLayout Component={DetailMovie} />} />
                </Routes>
            </BrowserRouter>
        </div>
    );
}

export default App;
