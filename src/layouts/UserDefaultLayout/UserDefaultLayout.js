import React from 'react';
import Footer from '../../components/Footer/Footer';
import Header from '../../components/Header/Header';

export default function UserDefaultLayout({ Component }) {
    return (
        <div className="container mx-auto">
            <Header />
            {<Component />}
            <Footer />
        </div>
    );
}
