const icons = {
    beta: require('./beta.jpg'),
    bhd: require('./bhd.png'),
    cgv: require('./cgv.png'),
    cinemax: require('./cinemax.jpg'),
    dicine: require('./dicine.png'),
    galaxy: require('./galaxy.png'),
    lotte: require('./lotte.png'),
    mega: require('./mego.png'),
    ppc: require('./ppc.png'),
    star: require('./star.png'),
    touch: require('./touch.png'),
    touch2: require('./touch2.png'),
};

export default icons;
