const images = {
    logo: require('./logo.png'),
    user: require('./user.png'),
    apple: require('./apple.png'),
    zalo: require('./zalo.png'),
    facebook: require('./facebook.png'),
    chplay: require('./chplay.png'),
    zion: require('./zion.jpg'),
    cerf: require('./cerf.png'),
    app1: require('./app-1.jpg'),
    app2: require('./app-2.jpg'),
    app3: require('./app-3.jpg'),
    app4: require('./app-4.jpg'),
    bgApp: require('./bgapp.jpg'),
    phone: require('./phone.png'),
};

export default images;
