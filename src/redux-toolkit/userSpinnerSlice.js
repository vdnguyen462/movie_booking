import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    isLoading: false,
};

const userSpinnerSlice = createSlice({
    name: 'userSpinnerSlice',
    initialState,
    reducers: {
        setLoadingOn: (state, action) => {
            state.isLoading = true;
        },
        setLoadingOff: (state, action) => {
            state.isLoading = false;
        },
    },
});

export const { setLoadingOn, setLoadingOff } = userSpinnerSlice.actions;

export default userSpinnerSlice.reducer;
