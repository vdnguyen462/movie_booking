import { createSlice } from '@reduxjs/toolkit';
import { localUserService } from '../services/localService';

const initialState = {
    userInfo: localUserService.get(),
};

const userSlice = createSlice({
    name: 'userSlice',
    initialState,
    reducers: {
        setLogin: (state, action) => {
            state.userInfo = action.payload;
        },
    },
});

export const { setLogin } = userSlice.actions;

export default userSlice.reducer;
