import React, { useEffect, useState } from 'react';
import ShowTimes from '../../components/ShowTimes/ShowTimes';

import { movieService } from '../../services/movieService';
import Banner from './Banner/Banner';
import ListMovie from './ListMovie/ListMovie';
import MyApp from './MyApp/MyApp';

export default function HomePage() {
    const [theaters, setTherters] = useState([]);

    useEffect(() => {
        movieService
            .getDataTheater()
            .then((res) => {
                setTherters(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <section>
            <Banner />
            <ListMovie />
            <ShowTimes theaters={theaters} />
            <MyApp />
        </section>
    );
}
