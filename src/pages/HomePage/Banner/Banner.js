import React, { useEffect, useState } from 'react';
import { Carousel } from 'flowbite-react';
import classNames from 'classnames/bind';
import styles from './Banner.module.scss';

import { movieService } from '../../../services/movieService';
import { LeftIcon, RightIcon } from '../../../components/Icons/Icons';
import MediaQuery from 'react-responsive';

const cx = classNames.bind(styles);

export default function Banner() {
    const [banner, setBanner] = useState([]);

    useEffect(() => {
        movieService
            .getBannerFirm()
            .then((res) => {
                setBanner(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <MediaQuery minWidth={300}>
            <div className={cx('banner')}>
                <Carousel
                    indicators={false}
                    leftControl={<LeftIcon className="text-white" />}
                    rightControl={<RightIcon className="text-white" />}
                >
                    {banner?.map((item) => {
                        return <img className={cx('bannerImg')} src={item.hinhAnh} alt="" key={item.maBanner} />;
                    })}
                </Carousel>
            </div>
        </MediaQuery>
    );
}
