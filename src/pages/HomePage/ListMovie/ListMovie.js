import React, { useEffect, useState } from 'react';
import { Pagination } from 'antd';
import { movieService } from '../../../services/movieService';
import ItemMovie from '../ItemMovie/ItemMovie';
import { setLoadingOn, setLoadingOff } from '../../../redux-toolkit/userSpinnerSlice';
import { useDispatch } from 'react-redux';
import MediaQuery from 'react-responsive';

export default function ListMovie() {
    const [listMovie, setListMovie] = useState([]);
    const [listMoviePerOnePage, setListMoviePerOnePage] = useState([]);

    const pageItem = 8;
    const [current, setCurrent] = useState(1);
    const dispatch = useDispatch();

    const onChange = (page) => {
        setCurrent(page);
    };

    useEffect(() => {
        dispatch(setLoadingOn());

        movieService
            .getListMoviePerOnePage(current, pageItem)
            .then((res) => {
                setListMoviePerOnePage(res.data.content);
                dispatch(setLoadingOff());
            })
            .catch((err) => {
                console.log(err);
            });

        movieService
            .getListFirm()
            .then((res) => {
                setListMovie(res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [current]);

    return (
        <>
            <MediaQuery minWidth={767}>
                <div id="showTimes" className="text-center">
                    <div className="lg:px-16 md:px-16 sm:mt-20">
                        <h2 className="text-start pb-4 lg:text-4xl md:text-4xl sm:text-2xl font-semibold">
                            Phim đang chiếu
                        </h2>
                        <div className="grid lg:grid-cols-3 md:grid-cols-2 content-center gap-4">
                            {listMoviePerOnePage.items?.map((item) => {
                                return <ItemMovie item={item} key={item.maPhim} />;
                            })}
                        </div>
                        <Pagination
                            className="pt-8"
                            current={current}
                            onChange={onChange}
                            total={listMovie.length}
                            defaultPageSize={pageItem}
                        />
                    </div>
                </div>
            </MediaQuery>
            <MediaQuery maxWidth={768}>
                <div id="showTimes" className="text-center">
                    <div className="lg:px-16 md:px-16 sm:mt-20">
                        <h2 className="text-start pb-4 lg:text-4xl md:text-4xl sm:text-xl font-semibold">
                            Phim đang chiếu
                        </h2>
                        <div className="grid lg:grid-cols-4 md:grid-cols-3 content-center gap-4">
                            {listMoviePerOnePage.items?.map((item) => {
                                return <ItemMovie item={item} key={item.maPhim} />;
                            })}
                        </div>
                        <Pagination
                            className="pt-8"
                            current={current}
                            onChange={onChange}
                            total={listMovie.length}
                            defaultPageSize={pageItem}
                        />
                    </div>
                </div>
            </MediaQuery>
        </>
    );
}
