import React, { useState } from 'react';
import classNames from 'classnames/bind';

import styles from './ModalTrailer.module.scss';

import { CloseIcon } from '../../../components/Icons/Icons';
import { Spin } from 'antd';

const cx = classNames.bind(styles);

export default function ModalTrailer({ src }) {
    const [modal, setModal] = useState(false);
    const [videoLoading, setVideoLoading] = useState(true);

    const openModal = () => {
        setModal(!modal);
    };

    const spinner = () => {
        setVideoLoading(!videoLoading);
    };

    return (
        <div className={cx('modal')}>
            <button
                onClick={openModal}
                className="transition ease-out focus:outline-none text-white bg-red-700 hover:bg-red-800 duration-150 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
            >
                <span>
                    {' '}
                    <i class="fa fa-film px-2"></i>
                </span>
                TRAILER
                {modal ? (
                    <section className={cx('modal__bg')}>
                        <div className={cx('modal__align')}>
                            <div className={cx('modal__content')} modal={modal}>
                                <CloseIcon className={cx('modal__close')} onClick={setModal} />
                                <div className={cx('modal__video-align')}>
                                    {videoLoading ? (
                                        <div className={cx('modal__spinner')}>
                                            <Spin size="large" />
                                        </div>
                                    ) : null}
                                    <iframe
                                        className={cx('modal__video-style')}
                                        onLoad={spinner}
                                        loading="lazy"
                                        width="800"
                                        height="500"
                                        src={`${src}?autoplay=1`}
                                        title="YouTube video player"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                        allowFullScreen
                                    ></iframe>
                                </div>
                            </div>
                        </div>
                    </section>
                ) : null}
            </button>
        </div>
    );
}
