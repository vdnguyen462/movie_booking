import React from 'react';
import { Card } from 'antd';
import { NavLink } from 'react-router-dom';
import ModalTrailer from './ModalTrailer';
import MediaQuery from 'react-responsive';
import classNames from 'classnames/bind';
import styles from './ModalTrailer.module.scss';
import './ItemMovie.css';

const cx = classNames.bind(styles);

const { Meta } = Card;
export default function ItemMovie({ item }) {
    return (
        <>
            <MediaQuery minWidth={767}>
                <div class="card">
                    <img class="image-cover" src={item.hinhAnh} alt="" />
                    <div class="card-overlay">
                        <div className="card-title text-xl font-bold tracking-wider leading-relaxed mt-5">
                            {item.tenPhim}
                        </div>
                        <div className="card-button">
                            <NavLink to={`/detail-movie/${item.maPhim}`}>
                                <button
                                    type="button"
                                    className="transition ease-in-out delay-150 over:-translate-y-1 hover:scale-110 hover:bg-red-500 hover:text-white duration-300 text-center rounded-lg p-2 bg-white  text-gray-700 font-bold text-base uppercase mr-3"
                                >
                                    xem chi tiết
                                </button>
                            </NavLink>
                            <NavLink to={`/buy-ticket`}>
                                <button class="transition ease-in-out delay-150 over:-translate-y-1 hover:scale-110 hover:bg-red-500 hover:text-white text-center rounded-lg p-2 bg-white  text-gray-700 font-bold text-base uppercase">
                                    mua vé
                                </button>
                            </NavLink>
                        </div>
                    </div>
                </div>
            </MediaQuery>
            <MediaQuery maxWidth={768}>
                <div class="card">
                    <img class="image-cover" src={item.hinhAnh} alt="" />
                    <div class="card-overlay">
                        <div className="card-title text-2xl font-bold tracking-wider leading-relaxed">
                            {item.tenPhim}
                        </div>
                        <div className="card-button">
                            <NavLink to={`/detail-movie/${item.maPhim}`}>
                                <button
                                    type="button"
                                    className="transition ease-in-out delay-150 over:-translate-y-1 hover:scale-110 hover:bg-red-500 hover:text-white duration-300 text-center rounded-lg p-2 bg-white  text-gray-700 font-bold text-lg uppercase"
                                >
                                    xem chi tiết
                                </button>
                            </NavLink>
                            <NavLink to={`/buy-ticket`}>
                                <button class="transition ease-in-out delay-150 over:-translate-y-1 hover:scale-110 hover:bg-red-500 hover:text-white text-center rounded-lg p-2 bg-white  text-gray-700 font-bold text-lg uppercase">
                                    mua vé
                                </button>
                            </NavLink>
                        </div>
                    </div>
                </div>
            </MediaQuery>
        </>
    );
}
