import React from 'react';
import classNames from 'classnames/bind';
import styles from './MyApp.module.scss';
import { Carousel } from 'antd';

import imgs from '../../../assets/images/imgs';

const cx = classNames.bind(styles);

export default function MyApp() {
    return (
        <div className="container mx-auto">
            <div id="myApp" className={cx('myApp')}>
                <div className={cx('content')}>
                    <div className={cx('link')}>
                        <h1 className={cx('title')}>
                            Ứng dụng tiện lợi dành cho
                            <br /> người yêu điện ảnh
                        </h1>
                        <span>Không chỉ đặt vé, bạn còn có thể bình luận phim, chấm điểm rạp và đổi quà hấp dẫn.</span>
                        <button className={cx('linkBtn')}>APP MIỄN PHÍ - TẢI VỀ NGAY</button>
                        <span>
                            TIX có hai phiên bản
                            <a className={cx('linkDownload')} href="">
                                IOS
                            </a>
                            và
                            <a className={cx('linkDownload')} href="">
                                Android
                            </a>
                        </span>
                    </div>
                    <div className={cx('carousel')}>
                        <img src={imgs.phone} alt="" className={cx('phoneBg')} />

                        <Carousel className={cx('carouselContent')} effect="fade" dots={false} autoplay={true}>
                            <div>
                                <img src={imgs.app1} alt="" className={cx('carouselImg')} />
                            </div>
                            <div>
                                <img src={imgs.app2} alt="" className={cx('carouselImg')} />
                            </div>
                            <div>
                                <img src={imgs.app3} alt="" className={cx('carouselImg')} />
                            </div>
                            <div>
                                <img src={imgs.app4} alt="" className={cx('carouselImg')} />
                            </div>
                        </Carousel>
                    </div>
                </div>
            </div>
        </div>
    );
}
