import React from 'react';
import { useDispatch } from 'react-redux';
import { setLoadingOff, setLoadingOn } from '../../../redux-toolkit/spinnerSlice';
import { movieService } from '../../../services/movieService';
import AddMovieForm from './utils';

export default function AddAdminMovie() {
    let dispatch = useDispatch();
    let handleAddUploadImage = (formData) => {
        dispatch(setLoadingOn());
        movieService
            .getAddImageMovie(formData)
            .then((res) => {
                dispatch(setLoadingOff());
                console.log(res.data.content);
            })
            .catch((err) => {
                dispatch(setLoadingOff());
                console.log(err.response?.data);
            });
    };
    return (
        <div>
            <AddMovieForm handleAddUploadImage={handleAddUploadImage} />
        </div>
    );
}
