import {
    Button,
    Cascader,
    DatePicker,
    Form,
    Input,
    InputNumber,
    message,
    Radio,
    Select,
    Switch,
    TreeSelect,
} from 'antd';
import { useFormik } from 'formik';
import moment from 'moment';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { setLoadingOn } from '../../../redux-toolkit/spinnerSlice';
import { GROUPID } from '../../../services/config';

const AddMovieForm = ({ handleAddUploadImage }) => {
    const [imgSrc, setImgSrc] = useState('');
    let dispatch = useDispatch();
    const formik = useFormik({
        initialValues: {
            tenPhim: '',
            trailer: '',
            moTa: '',
            ngayKhoiChieu: '',
            dangChieu: false,
            sapChieu: false,
            hot: false,
            danhGia: 0,
            hinhAnh: {},
        },
        onSubmit: (values) => {
            values.maNhom = GROUPID;
            let formData = new FormData();
            for (let key in values) {
                if (key != 'hinhAnh') {
                    formData.append(key, values[key]);
                } else {
                    formData.append('hinhAnh', values.hinhAnh, values.hinhAnh.name);
                }
            }
            dispatch(handleAddUploadImage(formData));
        },
    });

    const handleChangeDatePicker = (value) => {
        let ngayKhoiChieu = moment(value).format('DD/MM/YYYY');
        formik.setFieldValue('ngayKhoiChieu', ngayKhoiChieu);
    };

    const handleChangeSwitch = (name) => {
        return (value) => {
            formik.setFieldValue(name, value);
        };
    };

    const handleChangeInputNumber = (name) => {
        return (value) => {
            formik.setFieldValue(name, value);
        };
    };

    const handleChangeFile = (e) => {
        let file = e.target.files[0];
        if (
            file.type === 'image/jpeg' ||
            file.type === 'image/png' ||
            file.type === 'image/gif' ||
            file.type == 'image/bmp'
        ) {
            let reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = (e) => {
                setImgSrc(e.target.result);
            };
            formik.setFieldValue('hinhAnh', file);
        }
    };

    return (
        <Form
            onSubmitCapture={formik.handleSubmit}
            labelCol={{
                span: 10,
            }}
            wrapperCol={{
                span: 24,
            }}
            layout="vertical"
            style={{
                width: 'auto',
            }}
        >
            <Form.Item label="Tên phim">
                <Input name="tenPhim" onChange={formik.handleChange} />
            </Form.Item>
            <Form.Item label="Trailer">
                <Input name="trailer" onChange={formik.handleChange} />
            </Form.Item>
            <Form.Item label="Mô tả">
                <Input name="moTa" onChange={formik.handleChange} />
            </Form.Item>
            <Form.Item label="Ngày khởi chiếu">
                <DatePicker format={'DD/MM/YYYY'} onChange={handleChangeDatePicker} />
            </Form.Item>
            <Form.Item label="Đang chiếu">
                <Switch className="bg-gray-400" name="dangChieu" onChange={handleChangeSwitch('dangChieu')} />
            </Form.Item>
            <Form.Item label="Sắp chiếu">
                <Switch className="bg-gray-400" name="sapChieu" onChange={handleChangeSwitch('sapChieu')} />
            </Form.Item>
            <Form.Item label="Hot">
                <Switch className="bg-gray-400" name="hot" onChange={handleChangeSwitch('hot')} />
            </Form.Item>
            <Form.Item label="Đánh giá">
                <InputNumber onChange={handleChangeInputNumber('danhGia')} />
            </Form.Item>
            <Form.Item label="Hình ảnh">
                <input type="file" onChange={handleChangeFile} />
                <br />
                <img
                    style={{ width: 150, height: 150 }}
                    src={imgSrc}
                    alt="..."
                    accept="image/jpeg, image/png, image/gif, image/bmp"
                />
            </Form.Item>
            <Form.Item>
                <button
                    type="submit"
                    className="bg-blue-600 text-white p-2 rounded-lg hover:bg-blue-500 duration-500"
                    style={{ left: '25rem' }}
                >
                    Thêm phim
                </button>
            </Form.Item>
        </Form>
    );
};
export default AddMovieForm;
