import { DeleteOutlined, EditOutlined } from '@ant-design/icons';
import { Button, Input, message, Space, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useSearchParams } from 'react-router-dom';
import { setLoadingOff, setLoadingOn } from '../../redux-toolkit/spinnerSlice';
import { movieService } from '../../services/movieService';
import { headersMovies } from './utils';

export default function AdminMoviePage() {
    const [movies, setMovies] = useState([]);
    const [searchMovie, setSearchMovie] = useSearchParams();
    let dispatch = useDispatch();
    let fetchMovieList = () => {
        dispatch(setLoadingOn());
        movieService
            .getListMovie()
            .then((res) => {
                console.log('🚀 ~ file: AdminMoviePage.js:19 ~ .then ~ res:', res);
                dispatch(setLoadingOff());
                let movieArr = res.data.content.map((item) => {
                    return {
                        ...item,
                        action: (
                            <Space>
                                <a>
                                    <Button
                                        onClick={() => {
                                            handleEditMovie();
                                        }}
                                        type="primary"
                                        ghost
                                        icon={<EditOutlined />}
                                    />
                                </a>
                                <a>
                                    <Button
                                        onClick={() => {
                                            handleDeleteFilm(item.maPhim);
                                        }}
                                        type="primary"
                                        danger
                                        ghost
                                        icon={<DeleteOutlined />}
                                    />
                                </a>
                            </Space>
                        ),
                    };
                });
                setMovies(movieArr);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    useEffect(() => {
        fetchMovieList();
    }, []);

    let handleDeleteFilm = (MaPhim) => {
        dispatch(setLoadingOn());
        movieService
            .deleteMovie(MaPhim)
            .then((res) => {
                console.log('🚀 ~ file: AdminMoviePage.js:66 ~ .then ~ res:', res);
                dispatch(setLoadingOff());
                fetchMovieList();
                message.success('Xoá thành công');
            })
            .catch((err) => {
                console.log(err);
                message.error(err.response.data.content);
            });
    };

    let handleEditMovie = () => {
        console.log('Hello');
    };

    return (
        <div>
            <Table columns={headersMovies} dataSource={movies} />
        </div>
    );
}
