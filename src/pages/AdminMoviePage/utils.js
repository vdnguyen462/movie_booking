import { Button, Space, Tag } from 'antd';
import { DeleteOutlined, EditOutlined, SearchOutlined } from '@ant-design/icons';
import { adminService } from '../../services/adminService';

export const headersMovies = [
    {
        title: 'Mã Phim',
        dataIndex: 'maPhim',
        key: 'maPhim',
    },
    {
        title: 'Hình Ảnh',
        dataIndex: 'hinhAnh',
        key: 'hinhAnh',
        render: (hinhAnh) => {
            return <img src={hinhAnh} className="h-30 w-20 object-cover" />;
        },
    },
    {
        title: 'Tên Phim',
        dataIndex: 'tenPhim',
        key: 'tenPhim',
    },
    {
        title: 'Mô tả',
        dataIndex: 'moTa',
        key: 'moTa',
        render: (moTa) => {
            return <p className="w-60 text-gray-500 truncate ...">{moTa}</p>;
        },
    },
    {
        title: 'Trạng thái',
        dataIndex: 'dangChieu',
        key: 'dangChieu',
        render: (type) => {
            if (type == true) {
                return <Tag color="green">Đang chiếu</Tag>;
            } else {
                return <Tag color="yellow">Tạm dừng/Sắp chiếu</Tag>;
            }
        },
    },
    {
        title: 'Thao tác',
        key: 'MaPhim',
        dataIndex: 'action',
    },
];
