import React, { useEffect, useState } from 'react';
import classNames from 'classnames/bind';
import styles from './DetailMovie.module.scss';
import { NavLink, useParams } from 'react-router-dom';
import { movieService } from '../../services/movieService';
import moment from 'moment/moment';
import ModalTrailer from '../HomePage/ItemMovie/ModalTrailer';
import DetailMovieItem from './DetailMovieItem';
import { useDispatch } from 'react-redux';
import { setLoadingOff, setLoadingOn } from '../../redux-toolkit/userSpinnerSlice';
import { Rate } from 'antd';
import MediaQuery from 'react-responsive';

const cx = classNames.bind(styles);

export default function DetailMovie() {
    const { id } = useParams();
    const [movie, setMovie] = useState([]);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(setLoadingOn());
        movieService
            .getDataShowTimesMovie(id)
            .then((res) => {
                setMovie(res.data.content);
                dispatch(setLoadingOff());
                console.log('data', res.data.content);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    return (
        <div className="px-16 py-8">
            <MediaQuery minWidth={768}>
                <div className={cx('title')}>
                    <h1 className="text-5xl capitalize font-bold text-gray-600">nội dung phim</h1>
                    <hr class="h-1 my-8 bg-gray-200 border-0 dark:bg-gray-900"></hr>
                </div>
                <div className={cx('detailMovie')}>
                    <div className={cx('imgMovie')}>
                        <img className="w-full h-full rounded-lg" src={movie.hinhAnh} alt="" />
                    </div>
                    <div className="flex flex-col justify-between">
                        <h2 className="font-bold text-3xl uppercase">{movie.tenPhim}</h2>
                        <hr class="h-px bg-gray-200 my-1 dark:bg-gray-500"></hr>
                        <p className={cx('desc')}>
                            <strong>Đạo diễn</strong>:
                        </p>
                        <p className={cx('desc')}>
                            <strong>Diễn viên</strong>:
                        </p>
                        <p className={cx('desc')}>
                            <strong>Thể loại</strong>: Hành động, tâm lý
                        </p>
                        <p className={cx('desc')}>
                            <strong>Thời lượng</strong>: 130 phút
                        </p>
                        <p className={cx('desc')}>
                            <strong>Ngôn ngữ</strong>: Tiếng Anh - phụ đề Tiếng Việt
                        </p>
                        <p className={cx('desc')}>
                            <strong>Rated</strong>: T16 - Phim được phổ biến đến người xem từ đủ 16 tuổi trở lên (16+)
                        </p>
                        <p className={cx('desc')}>
                            <strong>Khởi chiếu</strong>: {moment(movie.ngayKhoiChieu).format('DD-MM-YYY')}
                        </p>
                        <div className="p-0 ml-0">
                            <NavLink to="/buy-ticket">
                                <button
                                    type="button"
                                    className="focus:outline-none text-white bg-red-700 hover:bg-red-800 font-medium rounded-lg text-sm px-5 py-2.5 ml-4 dark:bg-green-600 dark:hover:bg-green-700 dark:focus:ring-green-800"
                                >
                                    <span>
                                        <i class="fa fa-ticket-alt px-2"></i>
                                    </span>
                                    MUA VÉ
                                </button>
                            </NavLink>
                        </div>
                    </div>

                    <div className="flex flex-col items-center">
                        <div className="flex justify-center items-center h-32 w-32 rounded-full border-solid border-8 border-orange-500">
                            <span className="font-bold text-5xl">{movie.danhGia / 2}</span>
                        </div>
                        <Rate allowHalf value={movie.danhGia / 2} disabled={true} />
                    </div>
                </div>
                <div className="m-4">
                    <ModalTrailer src={movie.trailer} />
                </div>
                <div className="m-10 text-xl">
                    <p className={cx('desc')}>{movie.moTa}</p>
                </div>
                <div className="mt-8">
                    <DetailMovieItem theaters={movie.heThongRapChieu} />
                </div>
            </MediaQuery>
        </div>
    );
}
