import React from 'react';
import classNames from 'classnames/bind';
import styles from './DetailMovie.module.scss';
import { Tabs } from 'antd';
import moment from 'moment';

const cx = classNames.bind(styles);

export default function DetailMovieItem({ theaters }) {
    const renderTherters = () => {
        return theaters?.map((theater) => {
            return {
                key: theater.maHeThongRap,
                label: (
                    <div className={cx('theaterLogo')}>
                        <img src={theater.logo} alt="" className="h-12 w-12" />
                    </div>
                ),
                children: theater.cumRapChieu.map((item) => {
                    return (
                        <div className="pt-2 pr-4 pb-4">
                            <span className="text-xl font-medium py-2">{item.tenCumRap}</span>
                            <div className="grid lg:grid-cols-3 lg:gap-4 md:grid-cols-2 md:gap-2 mt-2">
                                {item.lichChieuPhim.map((time, index) => {
                                    return (
                                        <span
                                            key={index}
                                            className="border rounded px-1 py-3 hover:cursor-pointer text-center font-medium"
                                        >
                                            {moment(time.ngayChieuGioChieu).format('DD-MM-YYYY ~ hh:mm')}
                                        </span>
                                    );
                                })}
                            </div>
                        </div>
                    );
                }),
            };
        });
    };

    if (theaters?.length === 0) {
        return (
            <>
                <div id="theater" className="container px-8">
                    <h2 className="text-start pb-4 text-4xl font-semibold mb-4">Lịch Chiếu Phim</h2>
                    <span className="text-xl">Phim hiện chưa có lịch chiếu.</span>
                </div>
            </>
        );
    } else {
        return (
            <>
                <div id="theater" className="container px-8">
                    <h2 className="text-start pb-4 text-4xl font-semibold mb-4">Lịch Chiếu Phim</h2>

                    <div className="px-16">
                        <div className="mb-8 border">
                            <Tabs defaultActiveKey="1" tabPosition="left" items={renderTherters()} />
                        </div>
                    </div>
                </div>
            </>
        );
    }
}
