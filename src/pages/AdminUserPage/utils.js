import { Button, Space, Tag } from 'antd';
import { DeleteOutlined, EditOutlined, SearchOutlined } from '@ant-design/icons';

export const headersUsers = [
    {
        title: 'Tài khoản',
        dataIndex: 'taiKhoan',
        key: 'taiKhoan',
    },
    {
        title: 'Họ tên',
        dataIndex: 'hoTen',
        key: 'hoTen',
    },
    {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
    },
    {
        title: 'Loại người dùng',
        dataIndex: 'maLoaiNguoiDung',
        key: 'maLoaiNguoiDung',
        render: (type) => {
            if (type == 'KhachHang') {
                return <Tag color="blue">Khách hàng</Tag>;
            } else {
                return <Tag color="orange">Quản trị</Tag>;
            }
        },
    },
    {
        title: 'Thao tác',
        key: 'email',
        dataIndex: 'action',
    },
];
