import { Button } from 'antd';
import React from 'react';
import { adminService } from '../../../services/adminService';
import AddUserForm from './utils';

export default function AddAdminUser() {
    let handleAddUser = (e) => {
        let { value } = e.target;
        console.log('🚀 ~ file: AddAdminUser.js:9 ~ handleAddUser ~ value:', value);
        adminService
            .addUser()
            .then((res) => {
                console.log(res);
            })
            .catch((err) => {
                console.log(err);
            });
    };

    return (
        <div className="w-full h-full mt-auto mb-auto">
            <AddUserForm />
        </div>
    );
}
