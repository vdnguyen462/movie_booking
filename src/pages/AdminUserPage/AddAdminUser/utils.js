import { Button, Cascader, DatePicker, Form, Input, InputNumber, Radio, Select, Switch, TreeSelect } from 'antd';
import { useState } from 'react';
import './AddAdminUser.css';

let nameInput = (e) => {
    let { value } = e.target;
};
const AddUserForm = () => {
    return (
        <Form
            labelCol={{
                span: 10,
            }}
            wrapperCol={{
                span: 24,
            }}
            layout="vertical"
            style={{
                width: 'auto',
            }}
        >
            <Form.Item name="hoTen" label="Họ Tên" rules={[{ required: true }]}>
                <Input onChange={nameInput} />
            </Form.Item>
            <Form.Item name="email" label="Email" rules={[{ required: true, type: 'email' }]}>
                <Input onChange={nameInput} />
            </Form.Item>
            <Form.Item
                name="maLoaiNguoiDung"
                label="Loại Người Dùng"
                rules={[{ required: true }]}
                className="border-gray-700"
                initialValue="Khách Hàng"
            >
                <Select>
                    <Select.Option value="KhachHang">Khách Hàng</Select.Option>
                    <Select.Option value="QuanTri">Quản Trị</Select.Option>
                </Select>
            </Form.Item>
            <Form.Item name="soDt" label="Số điện thoại">
                <Input />
            </Form.Item>
            <Form.Item name="maNhom" label="Mã nhóm">
                <Input />
            </Form.Item>
            <Form.Item
                label="Username"
                name="taiKhoan"
                rules={[
                    {
                        required: true,
                        message: 'Please input your username!',
                    },
                ]}
            >
                <Input />
            </Form.Item>
            <Form.Item
                label="Password"
                name="matKhau"
                rules={[
                    {
                        required: true,
                        message: 'Please input your password!',
                    },
                ]}
            >
                <Input.Password />
            </Form.Item>
            <Button
                type="primary"
                ghost
                style={{
                    overflow: 'hidden',
                    width: '30%',
                    fontSize: '24px',
                    height: '40px',
                    margin: '0 auto',
                    display: 'block',
                }}
                clasname="btn-add-user"
            >
                Thêm người dùng
            </Button>
        </Form>
    );
};
export default AddUserForm;
