import { Button, Input, message, Space, Table } from 'antd';
import React, { useEffect, useState } from 'react';
import { adminService } from '../../services/adminService';
import { headersUsers } from './utils';
import { DeleteOutlined, EditOutlined, SearchOutlined } from '@ant-design/icons';
import Spinner from '../../components/Spinner/Spinner';
import { setLoadingOff, setLoadingOn } from '../../redux-toolkit/spinnerSlice';
import { useDispatch } from 'react-redux';
import { useParams, useSearchParams } from 'react-router-dom';

export default function AdminUserPage() {
    const [users, setUsers] = useState([]);
    const [searchUser, setSearchUser] = useSearchParams();

    let dispatch = useDispatch();

    let fetchUserList = () => {
        dispatch(setLoadingOn());
        adminService
            .getUserList()
            .then((res) => {
                dispatch(setLoadingOff());
                let usersArr = res.data.content.map((item) => {
                    return {
                        ...item,
                        action: (
                            <Space>
                                <a>
                                    <Button
                                        onClick={() => {
                                            handleEditUser();
                                        }}
                                        type="primary"
                                        ghost
                                        icon={<EditOutlined />}
                                    />
                                </a>
                                <a>
                                    <Button
                                        onClick={() => {
                                            handleDeleteUser(item.taiKhoan);
                                        }}
                                        type="primary"
                                        danger
                                        ghost
                                        icon={<DeleteOutlined />}
                                    />
                                </a>
                            </Space>
                        ),
                    };
                });
                setUsers(usersArr);
            })
            .catch((err) => {
                dispatch(setLoadingOff());
                console.log(err);
            });
    };
    useEffect(() => {
        fetchUserList();
    }, []);

    let handleDeleteUser = (taiKhoan) => {
        dispatch(setLoadingOn());
        adminService
            .deleteUser(taiKhoan)
            .then((res) => {
                dispatch(setLoadingOff());
                fetchUserList();
                message.success('Xoá thành công');
            })
            .catch((err) => {
                dispatch(setLoadingOff());
                message.error(err.response.data.content);
            });
    };
    let handleEditUser = () => {
        console.log('Hello');
    };
    let handleSearchOnChange = (e) => {
        let { value } = e.target;
        setSearchUser({
            keyword: value,
        });
        dispatch(setLoadingOn());
        adminService
            .getSearchUser(value)
            .then((res) => {
                dispatch(setLoadingOff());
                if (!value) {
                    fetchUserList();
                } else {
                    let usersArr = res.data.content.map((item) => {
                        return {
                            ...item,
                            action: (
                                <Space>
                                    <a>
                                        <Button
                                            onClick={() => {
                                                handleEditUser();
                                            }}
                                            type="primary"
                                            ghost
                                            icon={<EditOutlined />}
                                        />
                                    </a>
                                    <a>
                                        <Button
                                            onClick={() => {
                                                handleDeleteUser(item.taiKhoan);
                                            }}
                                            type="primary"
                                            danger
                                            ghost
                                            icon={<DeleteOutlined />}
                                        />
                                    </a>
                                </Space>
                            ),
                        };
                    });
                    setUsers(usersArr);
                }
            })
            .catch((err) => {
                dispatch(setLoadingOff());
                console.log(err);
            });
    };
    return (
        <div>
            <Input
                placeholder="Tìm kiếm thông tin người dùng"
                className="my-5 border-gray-400 placeholder:text-back 
                placeholder: font-medium focus: border-2 hover:border-gray-400 "
                onChange={handleSearchOnChange}
            />
            <Table dataSource={users} columns={headersUsers} />
        </div>
    );
}
