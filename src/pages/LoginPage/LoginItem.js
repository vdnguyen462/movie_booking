import React, { useState } from 'react';
import { Button, Divider, Form, Input, message, Modal, Select, Typography } from 'antd';
import { useDispatch } from 'react-redux';
import { NavLink, useNavigate } from 'react-router-dom';

import { userService } from '../../services/userService';
import { localUserService } from '../../services/localService';
import { setLogin } from '../../redux-toolkit/userSlice';

import classNames from 'classnames/bind';
import styles from './Login.module.scss';
import Lottie from 'lottie-react';
import bg_animate from '../../assets/login_animation.json';

const cx = classNames.bind(styles);

export default function LoginItem() {
    let dispatch = useDispatch();
    let navigate = useNavigate();

    const handleSignIn = (values) => {
        userService
            .postLogin(values)
            .then((res) => {
                localUserService.set(res.data.content);
                dispatch(setLogin(res.data.content));
                if (res.data.content.maLoaiNguoiDung == 'QuanTri') {
                    navigate('/admin-users');
                } else {
                    navigate('/');
                }
                message.success('Đăng nhập thành công!');
            })
            .catch((err) => {
                message.error('Đăng nhập thất bại');
                console.log(err);
            });
    };

    const handleSignUp = (values) => {
        userService
            .postSignUp(values)
            .then((res) => {
                message.success('Đăng ký thàng công!');
                window.location.reload();
            })
            .catch((err) => {
                console.log(err);
            });
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    const [open, setOpen] = useState(false);
    const showModal = () => {
        setOpen(true);
    };
    const handleCancel = () => {
        console.log('Clicked cancel button');
        setOpen(false);
    };
    return (
        <>
            <div className="bg-gray-50 dark:bg-gray-900">
                <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
                    <a href="#" className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white">
                        <Lottie animationData={bg_animate} loop={true} />
                    </a>
                    <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
                        <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
                            <Form
                                size="small"
                                layout="vertical"
                                name="formLogin"
                                onFinish={handleSignIn}
                                onFinishFailed={onFinishFailed}
                                autoComplete="off"
                            >
                                <Form.Item
                                    label="Username"
                                    name="taiKhoan"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập tài khoản!',
                                        },
                                    ]}
                                >
                                    <Input
                                        type="text"
                                        placeholder="Nhập tài khoản..."
                                        className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    />
                                </Form.Item>

                                <Form.Item
                                    label="Password"
                                    name="matKhau"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập mật khẩu!',
                                        },
                                    ]}
                                >
                                    <Input.Password
                                        className="bg-gray-50 border flex border-gray-300 text-gray-900 sm:text-xs rounded-lg focus:ring-primary-600 focus:border-primary-600  w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                        placeholder="Nhập mật khẩu..."
                                    />
                                </Form.Item>

                                <Button
                                    style={{ height: 40 }}
                                    type="primary"
                                    htmlType="submit"
                                    className="w-full text-white bg-blue-500 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-2xl px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800"
                                >
                                    Sign in
                                </Button>
                                <div className="text-center w-full">
                                    <p className="text-sm font-light text-gray-500 dark:text-gray-400">
                                        Don’t have an account yet?{' '}
                                        <a
                                            href="#"
                                            className="font-medium text-primary-600 hover:underline dark:text-primary-500"
                                            onClick={showModal}
                                        >
                                            Sign up
                                        </a>
                                    </p>
                                </div>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
            <div className={cx('formModal')}>
                <Modal open={open} onCancel={handleCancel} footer={null} centered={true}>
                    <Form
                        layout="vertical"
                        name="formSignUp"
                        onFinish={handleSignUp}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                    >
                        <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
                            Create and account
                        </h1>

                        <div className='className="p-6 space-y-4 md:space-y-6 sm:p-8"'>
                            <Form.Item
                                label="Username"
                                name="taiKhoan"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập tài khoản!',
                                    },
                                ]}
                                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                            >
                                <Input
                                    className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Nhập tài khoản..."
                                />
                            </Form.Item>

                            <Form.Item
                                label="Password"
                                name="matKhau"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập mật khẩu!',
                                    },
                                ]}
                                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                            >
                                <Input.Password
                                    className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 flex w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Nhập mật khẩu..."
                                />
                            </Form.Item>

                            <Form.Item
                                label="Name"
                                name="hoTen"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập họ và tên!',
                                    },
                                ]}
                                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                            >
                                <Input
                                    className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Full name"
                                />
                            </Form.Item>

                            <Form.Item
                                label="Phone number"
                                name="soDt"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập số điện thoại!',
                                    },
                                ]}
                                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                            >
                                <Input
                                    className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Phone number"
                                />
                            </Form.Item>

                            <Form.Item
                                label="Email"
                                name="email"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập email!',
                                    },
                                ]}
                                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                            >
                                <Input
                                    className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    placeholder="Email"
                                />
                            </Form.Item>

                            <Form.Item
                                label="Group"
                                name="maNhom"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập mã nhóm!',
                                    },
                                ]}
                                className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                            >
                                <Select placeholder="Group">
                                    <Select.Option value="GP00">GP00</Select.Option>
                                    <Select.Option value="GP01">GP01</Select.Option>
                                    <Select.Option value="GP02">GP02</Select.Option>
                                    <Select.Option value="GP03">GP03</Select.Option>
                                </Select>
                            </Form.Item>

                            <Button
                                type="link"
                                style={{ height: 40, color: 'white' }}
                                class="w-full text-white bg-blue-600 hover:bg-blue-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                htmlType="submit"
                            >
                                Create an account
                            </Button>
                            <p class="text-sm font-light text-gray-500 dark:text-gray-400">
                                Already have an account?{' '}
                                <a href="" class="font-medium text-primary-600 hover:underline dark:text-primary-500">
                                    Login here
                                </a>
                            </p>
                        </div>
                    </Form>
                </Modal>
            </div>
        </>
    );
}
