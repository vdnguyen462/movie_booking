import LoginItem from './LoginItem';
import classNames from 'classnames/bind';
import styles from './Login.module.scss';
import MediaQuery from 'react-responsive';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';

const cx = classNames.bind(styles);

export default function LoginPage() {
    return (
        <>
            <Header />
            <LoginItem />
            <Footer />
        </>
    );
}
