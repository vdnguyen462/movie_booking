import React from 'react';
import images from '../../assets/images/imgs';
import classNames from 'classnames/bind';
import styles from './Header.module.scss';
import UserMenu from './UserMenu';
import { NavLink } from 'react-router-dom';
import MediaQuery from 'react-responsive';

const cx = classNames.bind(styles);

export default function Header() {
    return (
        <header className="px-4 h-20 flex justify-between items-center z-10 bg-white top-0 left-0 w-full container mx-auto">
            <div className="h-full">
                <NavLink to="/">
                    <img className={cx('logoImg')} src={images.logo} alt="" />
                </NavLink>
            </div>
            <MediaQuery minWidth={992}>
                <div className="navBar">
                    <ul class="font-semibold flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
                        <li>
                            <a
                                href="#showTimes"
                                class="block py-2 pl-3 pr-4 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0 dark:text-white md:dark:text-blue-500 uppercase"
                                aria-current="page"
                            >
                                Lịch chiếu
                            </a>
                        </li>
                        <li>
                            <a
                                href="#theater"
                                class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent uppercase"
                            >
                                Cụm rạp
                            </a>
                        </li>
                        <li>
                            <a
                                href="#"
                                class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent uppercase"
                            >
                                Tin tức
                            </a>
                        </li>
                        <li>
                            <a
                                href="#myApp"
                                class="block py-2 pl-3 pr-4 text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent uppercase"
                            >
                                Ứng dụng
                            </a>
                        </li>
                    </ul>
                </div>
                <MediaQuery maxWidth={991}>
                    <></>
                </MediaQuery>
            </MediaQuery>
            <div className="flex items-center">
                <UserMenu />
            </div>
        </header>
    );
}
