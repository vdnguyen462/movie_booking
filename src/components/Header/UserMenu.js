import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { localUserService } from '../../services/localService';
import classNames from 'classnames/bind';
import styles from './Header.module.scss';
import images from '../../assets/images/imgs';
import MediaQuery from 'react-responsive';
import DrawerUserMenu from './DrawerUserMenu';
import { Space } from 'antd';
import { LoginIcon, LogoutIcon, BarIcon } from '../../components/Icons/Icons';

const cx = classNames.bind(styles);

export default function UserMenu() {
    const [open, setOpen] = useState(false);

    const showDrawer = () => {
        setOpen(true);
    };

    const onClose = () => {
        setOpen(false);
    };

    let userInfo = useSelector((state) => {
        return state.userSlice.userInfo;
    });

    const handleLogout = () => {
        localUserService.remove();
        window.location.href = '/';
    };

    const renderUserMenu = () => {
        if (userInfo !== null) {
            return (
                <>
                    <MediaQuery minWidth={992}>
                        <div className="userInfor flex items-center">
                            <img className="h-8 w-8 mr-2" src={images.user} alt="" />
                            <span>{userInfo.hoTen}</span>
                        </div>
                        <div className={cx('divide')}></div>
                        <button className={cx('btn')} onClick={handleLogout}>
                            <LogoutIcon className="mr-2" />
                            Log out
                        </button>
                    </MediaQuery>
                    <MediaQuery maxWidth={991}>
                        <Space>
                            <button onClick={showDrawer}>
                                <BarIcon />
                            </button>
                        </Space>
                        <DrawerUserMenu open={open} onClose={onClose} handleLogout={handleLogout} />
                    </MediaQuery>
                </>
            );
        } else {
            return (
                <>
                    <MediaQuery minWidth={992}>
                        <div className="user flex items-center">
                            <NavLink to="/login">
                                <button className={cx('btn')}>
                                    <LoginIcon className="mr-2" />
                                    Log in
                                </button>
                            </NavLink>
                            <div className={cx('divide')}></div>
                            <NavLink to="/login#">
                                <button className={cx('btn')}>
                                    <LogoutIcon className="mr-2" />
                                    Sign up
                                </button>
                            </NavLink>
                        </div>
                    </MediaQuery>
                    <MediaQuery maxWidth={991}>
                        <button onClick={showDrawer}>
                            <BarIcon />
                        </button>
                        <DrawerUserMenu open={open} onClose={onClose} handleLogout={handleLogout} />
                    </MediaQuery>
                </>
            );
        }
    };

    return <>{renderUserMenu()}</>;
}
