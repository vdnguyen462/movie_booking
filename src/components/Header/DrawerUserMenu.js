import { Drawer } from 'antd';
import { NavLink } from 'react-router-dom';
import { LoginIcon, LogoutIcon } from '../Icons/Icons';
import { useSelector } from 'react-redux';
import images from '../../assets/images/imgs';

export default function DrawerUserMenu({ open, onClose, handleLogout }) {
    let userInfo = useSelector((state) => {
        return state.userSlice.userInfo;
    });

    const renderDrawerMenu = () => {
        if (userInfo === null) {
            return (
                <>
                    <div className="flex flex-col">
                        <NavLink to="/login">
                            <button className="flex items-center">
                                <LoginIcon className="mr-4" />
                                Sign in
                            </button>
                        </NavLink>
                        <NavLink to="/signup">
                            <button className="flex items-center mt-2">
                                <LogoutIcon className="mr-4" />
                                Sign up
                            </button>
                        </NavLink>
                    </div>
                </>
            );
        } else {
            return (
                <>
                    <div className="flex flex-col">
                        <div className="flex items-center">
                            <img className="h-8 w-8 mr-2 ml-1" src={images.user} alt="" />
                            <span>{userInfo.hoTen}</span>
                        </div>
                    </div>

                    <button className="flex items-center mt-4" onClick={handleLogout}>
                        <LogoutIcon className="mr-2" width="2rem" height="2rem" />
                        Sign out
                    </button>
                </>
            );
        }
    };
    return (
        <div>
            <Drawer
                width={250}
                placement="left"
                open={open}
                onClose={onClose}
                closable={false}
                title={renderDrawerMenu()}
            >
                <div style={{ minHeight: 200 }} className="flex flex-col justify-between">
                    <a className="px-4 font-semibold hover:text-red-500 w-fit" href="#showTimes">
                        <span>Lịch chiếu</span>
                    </a>
                    <a className="px-4 font-semibold hover:text-red-500" href="#">
                        <span>Cụm rạp</span>
                    </a>
                    <a className="px-4 font-semibold hover:text-red-500" href="#">
                        <span>Tin tức</span>
                    </a>
                    <a className="px-4 font-semibold hover:text-red-500" href="#myApp">
                        <span>Ứng dụng</span>
                    </a>
                </div>
            </Drawer>
        </div>
    );
}
