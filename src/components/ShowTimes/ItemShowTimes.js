import moment from 'moment/moment';
import classNames from 'classnames/bind';
import styles from './ShowTimes.module.scss';
import React from 'react';

const cx = classNames.bind(styles);

export default function ItemShowTimes({ movie }) {
    return (
        <>
            <div className={cx('wrapperItem')}>
                <div className={cx('theaterItem')}>
                    <img src={movie.hinhAnh} alt="" className="h-32 w-24 rounded" />
                    <div>
                        <h4 className="pl-6 mb-2 font-bold">{movie.tenPhim.toUpperCase()}</h4>

                        <div className={cx('itemShowTimesDate')}>
                            {movie.lstLichChieuTheoPhim?.map((item, index) => {
                                return (
                                    <span
                                        key={index}
                                        className="border rounded p-3 hover:cursor-pointer w-40 text-center font-medium"
                                    >
                                        {moment(item.ngayChieuGioChieu).format('DD-MM-YYYY - hh:mm')}
                                    </span>
                                );
                            })}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
