import classNames from 'classnames/bind';
import styles from './ShowTimes.module.scss';
import { Tabs } from 'antd';
import ItemShowTimes from './ItemShowTimes';
import MediaQuery from 'react-responsive';

const cx = classNames.bind(styles);

export default function ShowTimes({ theaters }) {
    const renderTherters = () => {
        return theaters?.map((theater) => {
            return {
                key: theater.maHeThongRap,
                label: (
                    <div className="logo">
                        <img src={theater.logo} alt="" className="h-12 w-12" />
                    </div>
                ),
                children: (
                    <div className={cx('showTimesList')}>
                        <Tabs
                            defaultActiveKey="1"
                            tabPosition="left"
                            items={theater.lstCumRap?.map((item) => {
                                return {
                                    key: item.maCumRap,
                                    label: (
                                        <div className={cx('theater')}>
                                            <h4 className="text-left">{item.tenCumRap.toUpperCase()}</h4>
                                        </div>
                                    ),
                                    children: (
                                        <div className={cx('showTimesListItem')}>
                                            {item.danhSachPhim?.map((movie) => {
                                                return <ItemShowTimes movie={movie} key={movie.maPhim} />;
                                            })}
                                        </div>
                                    ),
                                };
                            })}
                        />
                    </div>
                ),
            };
        });
    };
    return (
        <>
            <MediaQuery minWidth={992}>
                <div id="theater" className="container px-8">
                    <h2 className="text-start pb-4 text-4xl font-semibold mb-4">Lịch Chiếu Phim</h2>
                    <div className="px-16">
                        <div className="mb-8 border">
                            <Tabs defaultActiveKey="1" tabPosition="left" items={renderTherters()} />
                        </div>
                    </div>
                </div>
            </MediaQuery>
        </>
    );
}
