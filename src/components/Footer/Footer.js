import React from 'react';
import icons from '../../assets/icons/icons';
import images from '../../assets/images/imgs';
import classNames from 'classnames/bind';
import styles from './Footer.module.scss';
import MediaQuery from 'react-responsive';
import { NavLink } from 'react-router-dom';

const cx = classNames.bind(styles);

export default function Footer() {
    return (
        <footer className="px-4 py-6 bg-gray-600 w-full container mx-auto">
            <MediaQuery minWidth={768}>
                <div className="link grid grid-cols-5 justify-between gap-10 pb-6 text-gray-400">
                    <div className="item">
                        <h3 className="mb-4 font-semibold text-white text-lg">TIX</h3>
                        <ul>
                            <li className="hover:text-gray-500 hover:cursor-pointer">FAQ</li>
                            <li className="hover:text-gray-500 hover:cursor-pointer">Brand Guidelines</li>
                        </ul>
                    </div>
                    <div className="item">
                        <h3 className="mb-4 font-semibold text-white text-lg">Chính sách</h3>
                        <ul>
                            <li className="hover:text-gray-500 hover:cursor-pointer">Thỏa thuận sử dụng</li>
                            <li className="hover:text-gray-500 hover:cursor-pointer">Chính sách bảo mật</li>
                        </ul>
                    </div>
                    <div className="item ">
                        <h3 className="mb-4 font-semibold text-white text-lg">Đối tác</h3>
                        <div className="icon grid grid-cols-4 gap-2">
                            {Object.keys(icons).map((icon, index) => {
                                return (
                                    <img
                                        key={index}
                                        className="h-8 w-8 rounded-full hover:cursor-pointer"
                                        src={icons[icon]}
                                        alt=""
                                    />
                                );
                            })}
                        </div>
                    </div>
                    <div className="mobile">
                        <h3 className="mb-4 font-semibold text-white text-lg">Mobile App</h3>
                        <div className="grid grid-cols-2">
                            <NavLink to="https://www.apple.com/vn/app-store/">
                                <img
                                    className="w-full h-full hover:cursor-pointer border-spacing-1"
                                    src={images.apple}
                                    alt=""
                                />
                            </NavLink>

                            <NavLink to="https://play.google.com/store/games?utm_source=apac_med&hl=vi-VN&utm_medium=hasem&utm_content=Oct0121&utm_campaign=Evergreen&pcampaignid=MKT-EDR-apac-vn-1003227-med-hasem-py-Evergreen-Oct0121-Text_Search_BKWS-BKWS%7CONSEM_kwid_43700074087465541_creativeid_446280875239_device_c&gclid=Cj0KCQjwn_OlBhDhARIsAG2y6zOftyD-x8SZubkoQqtUbN2NWaRX8bb3kGNa6mO2g-1-HNdrmozNNqgaAiGQEALw_wcB&gclsrc=aw.ds&pli=1">
                                <img
                                    className="w-full h-full hover:cursor-pointer border-spacing-1"
                                    src={images.chplay}
                                    alt=""
                                />
                            </NavLink>
                        </div>
                    </div>
                    <div className="social">
                        <h3 className="mb-4 font-semibold text-white text-lg">Social</h3>
                        <div className="text-2xl hover:cucursor-pointer">
                            <NavLink to="https://www.facebook.com/">
                                <i class="fab fa-facebook"></i>
                            </NavLink>

                            <i class="fab fa-viber ml-2"></i>
                        </div>
                    </div>
                </div>
                <div className={cx('divide')}></div>
                <div className="address py-6 flex text-white">
                    <img className="w-24 h-12" src={images.zion} alt="" />
                    <div className="px-4">
                        <h4 className="font-semibold mb-2">TIX - SẢN PHẨM CỦA CÔNG TY CỔ PHẦN ZION</h4>
                        <span>
                            Địa chỉ: Z06 Đường số 13, Phường Tân Thuận Đông, Quận 7, Tp. Hồ Chí Minh, Việt Nam.
                            <br />
                            Giấy chứng nhận đăng ký kinh doanh số: 0101659783, đăng ký thay đổi lần thứ 30, ngày 22
                            tháng 01 năm 2020 do Sở kế hoạch và đầu tư Thành phố Hồ Chí Minh cấp.
                            <br />
                            Số Điện Thoại (Hotline): 1900 545 436
                        </span>
                    </div>
                    <img className="w-32 h-12" src={images.cerf} alt="" />
                </div>
            </MediaQuery>
        </footer>
    );
}
