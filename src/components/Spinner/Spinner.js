import React from 'react';
import { useSelector } from 'react-redux';
import { ClipLoader } from 'react-spinners';

export default function Spinner() {
    let { isLoading } = useSelector((state) => state.spinnerSlice);
    return isLoading ? (
        <div className="h-screen w-screen fixed top-0 left-0 flex justify-center items-center z-50 backdrop-brightness-50">
            <ClipLoader color="#1B1B1E" size={50} speedMultiplier={1} />
        </div>
    ) : (
        <></>
    );
}
