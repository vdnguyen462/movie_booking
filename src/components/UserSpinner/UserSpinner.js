import { Spin } from 'antd';
import React from 'react';
import { useSelector } from 'react-redux';

export default function UserSpinner() {
    const { isLoading } = useSelector((state) => state.userSpinnerSlice);

    return isLoading ? (
        <div
            style={{ backgroundColor: 'rgba(235,244,255,0.3)' }}
            className="h-screen w-screen fixed top-0 left-0 flex items-center justify-center z-40"
        >
            <Spin size="large" />
        </div>
    ) : (
        <></>
    );
}
